/*
好友管理相关sql
有待完善
可添加属性
*/
DROP TABLE IF EXISTS neuhub.friend_relation;
CREATE TABLE neuhub.friend_relation
(
    id           int       NOT NULL AUTO_INCREMENT,
    userID       int       NOT NULL,
    friendID     int       NOT NULL,
    groupID      int       NOT NULL COMMENT '分组',
    alias        char(50)  NOT NULL COMMENT '备注',

    gmt_create   timestamp NOT NULL,
    gmt_modified timestamp NOT NULL,
    intimacy     int       NOT NULL,
    lastViewTime timestamp NOT NULL,
    PRIMARY KEY (id)
    #PRIMARY KEY (userID, friendID)
);
