-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: neuhub
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP TABLE IF EXISTS `moments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `moments` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `uid` int NOT NULL,
                           `time` char(40) NOT NULL,
                           `content` text,
                           `content_images` text,
                           `likePeople` text,
                           `transmitId` int DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moments`
--

LOCK TABLES `moments` WRITE;
/*!40000 ALTER TABLE `moments` DISABLE KEYS */;
INSERT INTO `moments` VALUES (49,4,'2022-05-06 10:11:17','沈阳怎么风这么大？😅',NULL,'4',NULL),(50,4,'2022-05-06 10:22:41','快递恢复了！好耶！','http://rb99yvmfm.hb-bkt.clouddn.com/Fn9pUkOXfagS24vSn1_lyew3L1Ag',NULL,NULL),(51,5,'2022-05-06 10:24:36','今天也是美美哒！！！😃','http://rb99yvmfm.hb-bkt.clouddn.com/FqUiwguP7qb5aJHRm9jjWnsXsWA_','5',NULL),(52,6,'2022-05-06 10:26:53','软件工程大作业真有趣！🤭😝',NULL,'6 3',NULL);
/*!40000 ALTER TABLE `moments` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `uid` int NOT NULL,
                            `time` char(20) NOT NULL,
                            `content` text NOT NULL,
                            `likePeople` text,
                            `mid` int NOT NULL,
                            PRIMARY KEY (`id`),
                            KEY `comments_moments_id_fk` (`mid`),
                            CONSTRAINT `comments_moments_id_fk` FOREIGN KEY (`mid`) REFERENCES `moments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (67,3,'2022-05-06 10:31:33','真是有改不完的Bug呢😅','3',52),(68,3,'2022-05-06 10:34:00','🤗','',50);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friends` (
                           `masterId` int NOT NULL,
                           `friendId` int NOT NULL,
                           `intimacy` int NOT NULL,
                           `lastViewTime` char(20) DEFAULT NULL,
                           PRIMARY KEY (`masterId`,`friendId`),
                           KEY `friends_users_id_fk_2` (`friendId`),
                           CONSTRAINT `friends_user_userID_fk` FOREIGN KEY (`friendId`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE,
                           CONSTRAINT `friends_user_userID_fk_2` FOREIGN KEY (`masterId`) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (3,4,11,'2022-05-06 10:57:34'),(3,5,24,'2022-05-05 21:12:27'),(3,6,22,'2022-05-06 10:57:09'),(4,3,13,'2022-05-04 11:32:56'),(5,3,18,'2022-05-03 19:01:20'),(6,3,14,'2022-05-02 09:25:46');
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `type` varchar(40) NOT NULL,
                            `getMsgName` varchar(40) NOT NULL,
                            `name` varchar(40) NOT NULL,
                            `time` char(20) NOT NULL,
                            `seen` tinyint(1) NOT NULL,
                            `id1` int DEFAULT '-1',
                            `id2` int DEFAULT '-1',
                            `id3` int DEFAULT '-1',
                            PRIMARY KEY (`id`),
                            KEY `messages_moments_id_fk` (`id1`),
                            KEY `messages_comments_id_fk` (`id2`),
                            KEY `messages_subcomments_id_fk` (`id3`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (157,'点赞','ming','ming','2022-05-06 10:11:22',0,49,-1,-1),(158,'点赞','hong','hong','2022-05-06 10:24:37',0,51,-1,-1),(159,'点赞','jiejie','jiejie','2022-05-06 10:26:55',0,52,-1,-1),(160,'点赞','jiejie','LilPunchline','2022-05-06 10:29:57',0,52,-1,-1),(161,'评论','jiejie','LilPunchline','2022-05-06 10:31:33',0,52,67,-1),(162,'点赞','LilPunchline','LilPunchline','2022-05-06 10:33:29',1,52,67,-1),(163,'评论','ming','LilPunchline','2022-05-06 10:34:00',0,50,68,-1),(164,'评论','LilPunchline','LilPunchline','2022-05-06 10:57:29',1,50,68,18);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moments`
--



--
-- Table structure for table `subcomments`
--

DROP TABLE IF EXISTS `subcomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subcomments` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `cid` int NOT NULL,
                               `uid` int NOT NULL,
                               `name2` varchar(40) DEFAULT NULL,
                               `time` char(20) NOT NULL,
                               `content` text NOT NULL,
                               `likePeople` text,
                               PRIMARY KEY (`id`),
                               KEY `subcomments_comments_id_fk` (`cid`),
                               CONSTRAINT `subcomments_comments_id_fk` FOREIGN KEY (`cid`) REFERENCES `comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcomments`
--

LOCK TABLES `subcomments` WRITE;
/*!40000 ALTER TABLE `subcomments` DISABLE KEYS */;
INSERT INTO `subcomments` VALUES (18,68,3,'','2022-05-06 10:57:29','😊','');
/*!40000 ALTER TABLE `subcomments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

# DROP TABLE IF EXISTS `user`;
# /*!40101 SET @saved_cs_client     = @@character_set_client */;
# /*!50503 SET character_set_client = utf8mb4 */;
# CREATE TABLE `user` (
#                         `userID` int NOT NULL AUTO_INCREMENT,
#                         `username` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
#                         `password` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
#                         `email` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
#                         PRIMARY KEY (`userID`) USING BTREE,
#                         UNIQUE KEY `username` (`username`) USING BTREE,
#                         UNIQUE KEY `email` (`email`) USING BTREE
# ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;
# /*!40101 SET character_set_client = @saved_cs_client */;
#
# --
# -- Dumping data for table `user`
# --
#
# LOCK TABLES `user` WRITE;
# /*!40000 ALTER TABLE `user` DISABLE KEYS */;
# INSERT INTO `user` VALUES (1,'admin','admin','863119860@qq.com'),(2,'qian_xiao_yi','123456','3113824230@qq.com'),(3,'LilPunchline','123456','zjgshlzjs@outlook.com'),(4,'ming','123456','1196462708@qq.com'),(5,'hong','123456','772164540@qq.com'),(6,'jiejie','123456','zjgshlzjs@126.com');
# /*!40000 ALTER TABLE `user` ENABLE KEYS */;
# UNLOCK TABLES;
# /*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
#
# /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
# /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
# /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
# /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
# /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
# /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
# /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-06 11:14:42
