-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS neuhub.`chat`;
CREATE TABLE neuhub.`chat`
(
    `messageID` int                                                           NOT NULL AUTO_INCREMENT,
    `fromID`      int                                                           NOT NULL,
    `toID`        int                                                           NOT NULL,
    `content`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    PRIMARY KEY (`messageID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC;

-- 聊天表建表语句暂时放在这里，后续合并