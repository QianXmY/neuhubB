# 记录开发过程中遇到的问题（坑）

### 1. 相应乱码
**现象：** 响应内容中文乱码

**原因：** 没有设置相应的编码为`uft-8`，那一行不知道为啥之前给删了


### 2. Result类使用报错
**现象：** 利用`JackJSON`转自定义`Result`类为`JSON`时报错

**原因：** 没有设置`getter()`，`JackJSON`无法获得内部数据

### 3. 前端请求跨域问题
**现象1：** 后端将数据发送给了前端，但是前端请求收到错误回复
```
Access to XMLHttpRequest at 'http://localhost:8080/neuhub/user/login' from origin 'http://localhost:3000' has been blocked by CORS policy: 
Response to preflight request doesn't pass access control check: No 'Access-Control-Allow-Origin' header is present on the requested resource.
```

**原因：** 前端发起http请求时，会因为URL和后端响应的URL ip地址 端口 协议不一致产生跨域问题。

**解决方案：** 采用axios自带的代理功能，简单理解就是将前端URL映射成和后端**相同的**ip、端口，然后前端返回数据后，将回复URL再映射成前端ip、接口

**参考链接：** 

[Vue 代理解决跨域问题的完整解决方案](https://segmentfault.com/a/1190000037557209) <br> [vue3+vite解决跨域](https://juejin.cn/post/6968740208632135694)

**现象2：** 后端发送给前端的cookies，在下次的前端请求中并不能被携带。

**原因：** 因为在后端设置了起始路径`/neuhub/api`，所以后端创建的cookie的path都是`/neuhub/api`。
但是前端到代理的路径映射设置的是`http://localhost:3000/API/*` => `http://127.0.0.1:8080/neuhub/*` ，所以映射过程因为和cookie的path`neuhub/api`不符，
所以cookies就被浏览器屏蔽掉了。所以请求中就没有先前的cookies了

**解决办法：** 修改前端路径映射关系为`http://localhost:3000/neuhub/*` => `http://127.0.0.1:8080/neuhub/*`

**参考链接：**
[cookie中path路径探讨](https://www.jianshu.com/p/0152e5dc99f4)

### 4. 前端发送的post请求失效

**现象：** 前端**post**请求中携带的数据**data**，后端无法接收（收到的数据都是null）

**原因：** 前端post请求携带数据分两种类型：params和data。params会把数据放到**url**里，就和get请求一样。后端接收请求就正常的路径映射接收参数就可以。
data会把数据放到请求体中，根据data数据类型（json/form-data/text）的不同，请求头（header）会设置不同的**Content-Type**，而针对不同的Content-Type，后端接收参数的方式也不同，
当这两者不匹配时就接收不到前端发送的参数了。本次前端使用的是json数据类型，而后端没有使用注解，只能接受params请求，或者**form-data**，**x-www-form-urlencoded**类型数据

**解决方案：** 调整前端的发送方式为form-data。

**参考链接：** [axios POST提交数据的三种请求方式写法](https://segmentfault.com/a/1190000015261229)