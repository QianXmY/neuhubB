package org.se.neuhub.chat.entity;

import java.io.Serializable;

/**
 * 消息实体类
 */
public class Message implements Serializable {
    private int messageID;
    private int fromID;
    private int toID;
    private String content;

    private static final long serialVersionUID = 1L;

    public int getMessageID() {
        return messageID;
    }

    public int getFromID() {
        return fromID;
    }

    public int getToID() {
        return toID;
    }

    public String getContent() {
        return content;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

    public void setFromID(int fromID) {
        this.fromID = fromID;
    }

    public void setToID(int toID) {
        this.toID = toID;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", messageID=").append(messageID);
        sb.append(", from=").append(fromID);
        sb.append(", to=").append(toID);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
