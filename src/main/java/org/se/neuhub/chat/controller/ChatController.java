package org.se.neuhub.chat.controller;

import org.se.neuhub.chat.entity.Message;
import org.se.neuhub.chat.service.ChatService;
import org.se.neuhub.common.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("message")
public class ChatController {

    @Resource
    private ChatService chatService;

    @GetMapping("getAllMessage")
    public List<Message> getAllMessage(int ID1, int ID2) {
        List<Message> messages = chatService.getAllMessage(ID1, ID2);
        return messages;
    }

    @PostMapping("sendMessage")
    public Message sendMessage(int fromID, int toID, String content) {
        Message message = chatService.sendMessage(fromID, toID, content);
        return message;
    }

    @PostMapping("removeMessage")
    public Result removeMessage(int messageID) {
        int i = chatService.removeMessage(messageID);
        System.out.println(i);
        if (i == 1) {
            return Result.success("成功撤回");
        } else {
            return Result.fail("撤回失败");
        }
    }

}
