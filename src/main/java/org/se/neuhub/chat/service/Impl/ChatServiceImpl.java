package org.se.neuhub.chat.service.Impl;

import org.se.neuhub.chat.dao.ChatDao;
import org.se.neuhub.chat.entity.Message;
import org.se.neuhub.chat.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 洪鑫城
 */
@Service("messageService")
public class ChatServiceImpl implements ChatService {

    private ChatDao chatDao;

    @Autowired
    public void setMessageDao(ChatDao chatDao) {
        this.chatDao = chatDao;
    }

    @Override
    public List<Message> getAllMessage(int ID1, int ID2) {
        return chatDao.queryForChat(ID1, ID2);
    }

    @Override
    public Message sendMessage(int fromID, int toID, String content) {
        Message message = new Message();
        message.setFromID(fromID);
        message.setToID(toID);
        message.setContent(content);
        chatDao.insert(message);
        return message;
    }

    @Override
    public int removeMessage(int messageID) {
        chatDao.deleteById(messageID);
        return 1;
    }
}
