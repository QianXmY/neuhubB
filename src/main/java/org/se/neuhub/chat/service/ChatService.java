package org.se.neuhub.chat.service;

import org.se.neuhub.chat.entity.Message;

import java.util.List;

/**
 * @author 洪鑫城
 */
public interface ChatService {

    /**
     * 获得所有聊天记录
     *
     * @param ID1 已方的ID
     * @param ID2 对方的ID
     * @return 消息表
     */
    List<Message> getAllMessage(int ID1, int ID2);

    /**
     * 发送消息
     * @param fromID 发送方ID
     * @param toID 接收方ID
     * @param content 发送内容
     * @return 消息对象
     */
    Message sendMessage(int fromID, int toID, String content);

    /**
     * 撤回消息
     * @param messageID 消息主键ID
     * @return 影响行数
     */
    int removeMessage(int messageID);

}
