package org.se.neuhub.chat.dao;

import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.chat.entity.Message;

import java.util.List;

@Mapper
public interface ChatDao {

    /**
     * 通过ID查询单条数据
     *
     * @param commentID 主键
     * @return 实例对象
     */
    Message queryByID(int commentID);

    /**
     * 查询聊天记录
     *
     * @param ID1 己方的ID
     * @param ID2 对方的ID
     * @return 对象列表
     */
    List<Message> queryForChat(int ID1, int ID2);

    /**
     * 插入message
     *
     * @param message 实例对象
     * @return 影响行数
     */
    int insert(Message message);

    /**
     * 通过主键删除数据
     *
     * @param messageID 主键
     * @return 影响行数
     */
    int deleteById(int messageID);

}
