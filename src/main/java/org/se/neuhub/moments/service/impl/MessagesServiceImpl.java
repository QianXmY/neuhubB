package org.se.neuhub.moments.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.se.neuhub.moments.mapper.MessagesMapper;
import org.se.neuhub.moments.pojo.Messages;
import org.se.neuhub.moments.service.MessagesService;
import org.springframework.stereotype.Service;

/**
* @author Admin
* @description 针对表【messages】的数据库操作Service实现
* @createDate 2022-04-28 16:23:29
*/
@Service
public class MessagesServiceImpl extends ServiceImpl<MessagesMapper, Messages>
    implements MessagesService{

}




