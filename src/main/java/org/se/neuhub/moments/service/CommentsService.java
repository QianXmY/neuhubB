package org.se.neuhub.moments.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.se.neuhub.moments.pojo.Comments;

/**
* @author Admin
* @description 针对表【comments】的数据库操作Service
* @createDate 2022-04-28 16:23:29
*/
public interface CommentsService extends IService<Comments> {

}
