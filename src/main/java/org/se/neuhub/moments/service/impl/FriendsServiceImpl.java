package org.se.neuhub.moments.service.impl;

import com.github.jeffreyning.mybatisplus.service.MppServiceImpl;
import org.se.neuhub.moments.mapper.FriendsMapper;
import org.se.neuhub.moments.pojo.Friends;
import org.se.neuhub.moments.service.FriendsService;
import org.springframework.stereotype.Service;

/**
* @author Admin
* @description 针对表【friends】的数据库操作Service实现
* @createDate 2022-04-28 12:36:25
*/
@Service
public class FriendsServiceImpl extends MppServiceImpl<FriendsMapper, Friends>
    implements FriendsService{

}




