package org.se.neuhub.moments.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.se.neuhub.moments.mapper.CommentsMapper;
import org.se.neuhub.moments.pojo.Comments;
import org.se.neuhub.moments.service.CommentsService;
import org.springframework.stereotype.Service;

/**
* @author Admin
* @description 针对表【comments】的数据库操作Service实现
* @createDate 2022-04-28 16:23:29
*/
@Service
public class CommentsServiceImpl extends ServiceImpl<CommentsMapper, Comments>
    implements CommentsService{

}




