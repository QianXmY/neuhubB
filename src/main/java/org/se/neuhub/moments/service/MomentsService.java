package org.se.neuhub.moments.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.se.neuhub.moments.pojo.Moments;

/**
* @author Admin
* @description 针对表【moments】的数据库操作Service
* @createDate 2022-04-28 16:23:29
*/
public interface MomentsService extends IService<Moments> {

}
