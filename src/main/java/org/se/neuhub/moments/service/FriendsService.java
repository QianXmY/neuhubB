package org.se.neuhub.moments.service;

import com.github.jeffreyning.mybatisplus.service.IMppService;
import org.se.neuhub.moments.pojo.Friends;

/**
* @author Admin
* @description 针对表【friends】的数据库操作Service
* @createDate 2022-04-28 12:36:25
*/
public interface FriendsService extends IMppService<Friends> {

}
