package org.se.neuhub.moments.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.se.neuhub.moments.mapper.SubcommentsMapper;
import org.se.neuhub.moments.pojo.Subcomments;
import org.se.neuhub.moments.service.SubcommentsService;
import org.springframework.stereotype.Service;

/**
* @author Admin
* @description 针对表【subcomments】的数据库操作Service实现
* @createDate 2022-04-28 16:24:50
*/
@Service
public class SubcommentsServiceImpl extends ServiceImpl<SubcommentsMapper, Subcomments>
    implements SubcommentsService{

}




