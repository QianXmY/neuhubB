package org.se.neuhub.moments.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.se.neuhub.moments.mapper.MomentsMapper;
import org.se.neuhub.moments.pojo.Moments;
import org.se.neuhub.moments.service.MomentsService;
import org.springframework.stereotype.Service;

/**
* @author Admin
* @description 针对表【moments】的数据库操作Service实现
* @createDate 2022-04-28 16:23:29
*/
@Service
public class MomentsServiceImpl extends ServiceImpl<MomentsMapper, Moments>
    implements MomentsService{

}




