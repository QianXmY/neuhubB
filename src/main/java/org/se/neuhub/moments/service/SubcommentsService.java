package org.se.neuhub.moments.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.se.neuhub.moments.pojo.Subcomments;

/**
* @author Admin
* @description 针对表【subcomments】的数据库操作Service
* @createDate 2022-04-28 16:24:50
*/
public interface SubcommentsService extends IService<Subcomments> {

}
