package org.se.neuhub.moments.mapper;

import com.github.jeffreyning.mybatisplus.base.MppBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.moments.pojo.Friends;

/**
* @author Admin
* @description 针对表【friends】的数据库操作Mapper
* @createDate 2022-04-28 12:36:25
* @Entity com.neuhub.moments.pojo.Friends
*/
@Mapper
public interface FriendsMapper extends MppBaseMapper<Friends> {


}




