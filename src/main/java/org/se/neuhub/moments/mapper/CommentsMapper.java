package org.se.neuhub.moments.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.moments.pojo.Comments;

/**
* @author Admin
* @description 针对表【comments】的数据库操作Mapper
* @createDate 2022-04-28 16:23:29
* @Entity com.neuhub.moments.pojo.Comments
*/
@Mapper
public interface CommentsMapper extends BaseMapper<Comments> {

}




