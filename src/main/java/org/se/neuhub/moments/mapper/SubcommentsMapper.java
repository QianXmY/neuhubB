package org.se.neuhub.moments.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.moments.pojo.Subcomments;

/**
* @author Admin
* @description 针对表【subcomments】的数据库操作Mapper
* @createDate 2022-04-28 16:24:50
* @Entity com.neuhub.moments.pojo.Subcomments
*/
@Mapper
public interface SubcommentsMapper extends BaseMapper<Subcomments> {

}




