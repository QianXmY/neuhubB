package org.se.neuhub.moments.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.moments.pojo.Messages;

/**
* @author Admin
* @description 针对表【messages】的数据库操作Mapper
* @createDate 2022-04-28 16:23:29
* @Entity com.neuhub.moments.pojo.Messages
*/
@Mapper
public interface MessagesMapper extends BaseMapper<Messages> {

}




