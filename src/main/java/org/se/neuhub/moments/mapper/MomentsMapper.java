package org.se.neuhub.moments.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.se.neuhub.moments.pojo.Moments;

/**
* @author Admin
* @description 针对表【moments】的数据库操作Mapper
* @createDate 2022-04-28 16:23:29
* @Entity com.neuhub.moments.pojo.Moments
*/
@Mapper
public interface MomentsMapper extends BaseMapper<Moments> {

}




