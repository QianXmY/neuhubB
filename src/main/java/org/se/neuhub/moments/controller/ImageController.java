package org.se.neuhub.moments.controller;

import org.se.neuhub.common.util.ImageUtils;
import com.qiniu.http.Response;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Description: 图片的上传和删除
 * Author: 周靖森
 * DateTime: 2022/5/3
 */
@RestController
@RequestMapping("/moments")
public class ImageController {

    @PostMapping("/picPreview")
    @ResponseBody
    public Map<String, String> upload(@RequestParam("file") MultipartFile multipartFile){
        try {
            ImageUtils imageUtils = new ImageUtils();
            String name = imageUtils.upload(multipartFile, null);
            Map<String, String> map = new HashMap<>();
            map.put("name", name);
            map.put("url", ImageUtils.getUrl(name));
            System.out.println(map);
            return map;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @PostMapping("/picRemove")
    @ResponseBody
    public Response delete(@RequestBody Map<String,String> params) {
        try{
            ImageUtils imageUtils = new ImageUtils();
            return imageUtils.delete(params.get("url"));
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
