package org.se.neuhub.moments.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.se.neuhub.account.dao.UserDao;
import org.se.neuhub.account.entity.User;
import org.se.neuhub.common.global.CONSTANTS;
import org.se.neuhub.moments.pojo.*;
import org.se.neuhub.moments.service.*;
import org.se.neuhub.common.util.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * Description: 动态的控制层
 * Author: 周靖森
 * DateTime: 2022/5/3
 */

@RestController
@RequestMapping("/moments")
public class ResInfoController {

    final CommentsService commentsService;
    final FriendsService friendsService;
    final MessagesService messagesService;
    final MomentsService momentsService;
    final SubcommentsService subcommentsService;
    final UserDao userDao;

    @Autowired
    public ResInfoController(CommentsService commentsService,
                             FriendsService friendsService,
                             MessagesService messagesService,
                             MomentsService momentsService,
                             SubcommentsService subcommentsService, UserDao userDao) {
        this.commentsService = commentsService;
        this.friendsService = friendsService;
        this.messagesService = messagesService;
        this.momentsService = momentsService;
        this.subcommentsService = subcommentsService;
        this.userDao = userDao;
    }

    private final int C_moments = 6;
    private final int C_lastViewFriends = 3;
    private final int C_mostIntimacyFriends = 3;
    private final int C_lastViewFriendsMoments = 4;
    private final int C_messages = 8;

    public String getUserNameByUserId(long uid){
        return userDao.queryById(uid).getUsername();
    }

    public Long getUserIdByName(String name){
        return userDao.queryByUsername(name).getUserID();
    }

    public ResInfo.currentUser getCurrentUserByUserId(long uid){
        ResInfo.currentUser currentUser = new ResInfo.currentUser();
        User user = userDao.queryById(uid);
        currentUser.setImgsrc(ImageUtils.getUrl(user.getUsername()));
        System.out.println(currentUser.getImgsrc());
        currentUser.setName(user.getUsername());
        return currentUser;
    }

    public Collection<Long> getFriendsIdsByUserId(boolean includingSelf, boolean mostView, int num, long uid){
        QueryWrapper<Friends> wrapper = new QueryWrapper<>();
        wrapper.eq("masterId", uid);
        if (mostView){
            wrapper.orderByDesc("intimacy");
        }
        if (num>0){
            wrapper.last("limit "+num);
        }
        Collection<Long> collection = new ArrayList<>();
        for(Friends friend:friendsService.list(wrapper)){
            collection.add(friend.getFriendid());
        }
        if (includingSelf){
            collection.add(uid);
        }
        return collection;
    }

    public List<ResInfo.friend> getOrderedFriendsByUserId(long uid, int num, String col){
        QueryWrapper<Friends> wrapper = new QueryWrapper<>();
        if (num>0){
            wrapper.eq("masterId", uid).orderByDesc(col).last("limit "+num);
        }
        else {
            wrapper.eq("masterId", uid).orderByDesc(col);
        }
        List<Friends> friendsList = friendsService.list(wrapper);
        List<ResInfo.friend> iniFriendList = new ArrayList<>();
        for(Friends friend:friendsList){
            ResInfo.friend f = new ResInfo.friend();
            User user = userDao.queryById(friend.getFriendid());
            f.setImgsrc(ImageUtils.getUrl(user.getUsername()));
            f.setName(user.getUsername());
            f.setLastViewTime(friend.getLastviewtime());
            f.setIntimacy(friend.getIntimacy());
            iniFriendList.add(f);
        }
        return iniFriendList;
    }

    public List<String> departLikePeople(boolean getName,String likePeople){
        if(likePeople!=null){
            List<String> nameList1 = Arrays.asList(likePeople.split(" "));
            if(getName){
                List<String> nameList2 = new ArrayList<>();
                for (String str:nameList1){
                    if(!Objects.equals(str, ""))
                        nameList2.add(getUserNameByUserId(Integer.parseInt(str)));
                }
                nameList1 = nameList2;
            }
            return nameList1;
        }else{
            return new ArrayList<>();
        }
    }

    public ResInfo.subComment setResInfoSubComment(Subcomments sub){
        ResInfo.subComment res = new ResInfo.subComment();
        ResInfo.currentUser currentUser = getCurrentUserByUserId(sub.getUid());
        res.setId(sub.getId());
        res.setName(currentUser.getName());
        res.setName2(sub.getName2());
        res.setImgsrc(currentUser.getImgsrc());
        res.setContent(sub.getContent());
        res.setTime(sub.getTime());
        res.setLikePeople(departLikePeople(true,sub.getLikepeople()));
        return res;
    }

    public List<ResInfo.subComment> getSubCommentsByCommentId (int cid){
        QueryWrapper<Subcomments> wrapper = new QueryWrapper<>();
        wrapper.eq("cid", cid);
        List<Subcomments> subCommentsList = subcommentsService.list(wrapper);
        List<ResInfo.subComment> iniSubCommonList = new ArrayList<>();
        for (Subcomments s:subCommentsList){
            iniSubCommonList.add(setResInfoSubComment(s));
        }
        return iniSubCommonList;
    }

    public ResInfo.comment setResInfoComment(Comments comment){
        ResInfo.comment res = new ResInfo.comment();
        ResInfo.currentUser currentUser = getCurrentUserByUserId(comment.getUid());
        res.setId(comment.getId());
        res.setName(currentUser.getName());
        res.setImgsrc(currentUser.getImgsrc());
        res.setTime(comment.getTime());
        res.setLikePeople(departLikePeople(true, comment.getLikepeople()));
        res.setSub(getSubCommentsByCommentId(comment.getId()));
        res.setContent(comment.getContent());
        return res;
    }

    public List<ResInfo.comment> getCommentsByMomentId (int mid){
        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        wrapper.eq("mid", mid);
        List<Comments> commentsList = commentsService.list(wrapper);
        List<ResInfo.comment> iniCommentsList = new ArrayList<>();
        for (Comments c:commentsList){
            iniCommentsList.add(setResInfoComment(c));
        }
        return iniCommentsList;
    }

    public ResInfo.moment setResInfoMoment(Moments moment){
        ResInfo.moment res = new ResInfo.moment();
        ResInfo.currentUser user = getCurrentUserByUserId(moment.getUid());
        res.setId(moment.getId());
        res.setName(user.getName());
        res.setContent(moment.getContent());
        res.setTime(moment.getTime());
        res.setLikePeople(departLikePeople(true, moment.getLikepeople()));
        res.setImgsrc(user.getImgsrc());
        if(moment.getContentImages()!=null)
            res.setContent_images(Arrays.asList(moment.getContentImages().split(" ")));
        if (moment.getTransmitid()!=null){
            Moments transmitMoment = new Moments();
            transmitMoment.setId(moment.getTransmitid());
            QueryWrapper<Moments> wrapper = new QueryWrapper<>(transmitMoment);
            ResInfo.TransmitMoment t = new ResInfo.TransmitMoment();
            if(momentsService.count(wrapper)==1){
                transmitMoment = momentsService.getOne(wrapper);
                user = getCurrentUserByUserId(transmitMoment.getUid());
                t.setContent(transmitMoment.getContent());
                t.setId(transmitMoment.getId());
                t.setName(user.getName());
                t.setImgsrc(user.getImgsrc());
                if(transmitMoment.getContentImages()!=null)
                    t.setContent_images(Arrays.asList(transmitMoment.getContentImages().split(" ")));
            }else{
                t.setImgsrc("http://pic.sheji1688.net/element_origin_min_pic/16/09/" +
                        "13/1857d7cef34223b.jpg!/fw/700/quality/90/unsharp/true/compress/true");
                t.setName("该内容已失效");
            }
            res.setTransmitMoment(t);
        }
        res.setComments(getCommentsByMomentId(moment.getId()));
        return res;
    }

    public List<ResInfo.moment> setResInfoMomentsByWrapper(QueryWrapper<Moments> wrapper){
        List<Moments> momentsList = momentsService.list(wrapper);
        List<ResInfo.moment> iniMomentsList = new ArrayList<>();
        for(Moments moment:momentsList){
            iniMomentsList.add(setResInfoMoment(moment));
        }
        return iniMomentsList;
    }

    public List<ResInfo.moment>  getOrderedLimitedMomentsByUserIds
            (Collection<Long> uidCollection, int start, int end, String col){
        if(uidCollection.size()>0){
            QueryWrapper<Moments> wrapper = new QueryWrapper<>();
            wrapper.in("uid",uidCollection).orderByDesc(col).last("limit "+start+","+end);
            return setResInfoMomentsByWrapper(wrapper);
        }else return new ArrayList<>();
    }

    public List<ResInfo.moment>  getOrderedLimitedMomentsByUserId(long uid, int start, int end, String col){
        QueryWrapper<Moments> wrapper = new QueryWrapper<>();
        wrapper.eq("uid",uid).orderByDesc(col).last("limit "+start+","+end);
        return setResInfoMomentsByWrapper(wrapper);
    }

    public List<Messages> getOrderedLimitedMessagesByUserId
            (long uid, int start,int end, String col) {
        String name = getUserNameByUserId(uid);
        QueryWrapper<Messages> wrapper = new QueryWrapper<>();
        wrapper.eq("getMsgName",name).orderByDesc(col).last("limit "+start+","+end);
        return messagesService.list(wrapper);
    }

    public int getNewMessagesCount(long uid){
        String name = getUserNameByUserId(uid);
        QueryWrapper<Messages> wrapper = new QueryWrapper<>();
        wrapper.eq("getMsgName",name).eq("seen",0);
        return (int) messagesService.count(wrapper);
    }
    public ResInfo setResInfo(long uid){
        ResInfo resInfo = new ResInfo();
        resInfo.setCurrentUser(getCurrentUserByUserId(uid));
        resInfo.setFriends(getOrderedFriendsByUserId(uid,-1,"intimacy"));
        resInfo.setLastViewFriends(getOrderedFriendsByUserId(uid,C_lastViewFriends,"lastViewTime"));
        resInfo.setMostViewFriends(getOrderedFriendsByUserId(uid,C_mostIntimacyFriends,"intimacy"));
        resInfo.setMoments(new ArrayList<>());
//        resInfo.setMoments(getOrderedLimitedMomentsByUserIds
//                    (getFriendsIdsByUserId(true,false,-1,uid),0,C_moments,"time"));
        resInfo.setMessages(getOrderedLimitedMessagesByUserId(uid,0,C_messages,"time"));
        resInfo.setNewMessagesCount(getNewMessagesCount(uid));
        resInfo.setMostViewFriendsMoments(getOrderedLimitedMomentsByUserIds
                ((getFriendsIdsByUserId(false,true,-1,uid)),
                        0, C_lastViewFriendsMoments, "time"));
        return resInfo;
    }

    public void setFriend(long masterId, long friendId, String lastViewTime){
        Friends friend = new Friends();
        friend.setMasterid(masterId);
        friend.setFriendid(friendId);
        friend = friendsService.selectByMultiId(friend);
        friend.setIntimacy(friend.getIntimacy()+1);
        friend.setLastviewtime(lastViewTime);
        friendsService.updateByMultiId(friend);
    }

    public  String updateLikePeopleString(long uid, List<String> depart){
        String str = String.valueOf(uid);
        List<String> newDepart;
        if (depart!=null){
            newDepart = new ArrayList<>(depart);
        }
        else{
            newDepart = new ArrayList<>();
        }
        if (newDepart.contains(str)){
            newDepart.remove(str);
        }else {
            newDepart.add(str);
        }
        StringBuilder builder = new StringBuilder();
        if(newDepart.size()>0){
            for (String s:newDepart){
                if(!Objects.equals(s, "")){
                    builder.append(s);
                    builder.append(" ");
                }
            }
            builder.deleteCharAt(builder.length()-1);
        }
        return builder.toString();
    }

    public List<String> updateLikePeople(long uid, int mid, int cid, int sid){
        List<String> depart;
        List<String> new_depart;
        if (sid!=-1){
            Subcomments obj = subcommentsService.getById(sid);
            depart = departLikePeople(false,obj.getLikepeople());
            obj.setLikepeople(updateLikePeopleString(uid, depart));
            subcommentsService.updateById(obj);
            new_depart = departLikePeople(true, obj.getLikepeople());
        }else if(cid!=-1){
            Comments obj = commentsService.getById(cid);
            depart = departLikePeople(false,obj.getLikepeople());
            obj.setLikepeople(updateLikePeopleString(uid, depart));
            commentsService.updateById(obj);
            new_depart = departLikePeople(true, obj.getLikepeople());
        }else{
            Moments obj = momentsService.getById(mid);
            depart = departLikePeople(false,obj.getLikepeople());
            obj.setLikepeople(updateLikePeopleString(uid, depart));
            momentsService.updateById(obj);
            new_depart = departLikePeople(true, obj.getLikepeople());
        }
        return new_depart;
    }

    public String getGetMsgName(int mid, int cid, int sid){
        String name;
        if(sid!=-1){
            Subcomments obj = subcommentsService.getById(sid);
            name = getUserNameByUserId(obj.getUid());
        } else if (cid != -1) {
            Comments obj = commentsService.getById(cid);
            name = getUserNameByUserId(obj.getUid());
        }else {
            Moments obj = momentsService.getById(mid);
            name = getUserNameByUserId(obj.getUid());
        }
        return name;
    }

    public void updateMessage(long uid, int mid, int cid, int sid, String type, String getMsgName, String time){
        Messages message = new Messages();
        message.setGetmsgname(getMsgName);
        message.setName(getUserNameByUserId(uid));
        message.setId1(mid);
        message.setId2(cid);
        message.setId3(sid);
        message.setType(type);
        QueryWrapper<Messages> wrapper = new QueryWrapper<>(message);
        if (messagesService.count(wrapper)==0){
            message.setTime(time);
            message.setSeen(0);
            messagesService.save(message);
        }else{
            messagesService.remove(wrapper);
        }
    }

    public Comments addComment(long uid, int mid, String time, String content){
        Comments comment = new Comments();
        comment.setLikepeople("");
        comment.setContent(content);
        comment.setTime(time);
        comment.setMid(mid);
        comment.setUid(uid);
        commentsService.save(comment);
        QueryWrapper<Comments> wrapper = new QueryWrapper<>(comment);
        return commentsService.getOne(wrapper);
    }

    public Subcomments addSubComment(long uid, int cid, String time, String content, String name2){
        Subcomments sub = new Subcomments();
        sub.setLikepeople("");
        sub.setContent(content);
        sub.setTime(time);
        sub.setCid(cid);
        sub.setName2(name2);
        sub.setUid(uid);
        subcommentsService.save(sub);
        QueryWrapper<Subcomments> wrapper = new QueryWrapper<>(sub);
        return subcommentsService.getOne(wrapper);
    }

    public void delete(int mid, int cid, int sid){
        if (sid!=-1){
            subcommentsService.removeById(sid);
        }else if (cid!=-1){
            commentsService.removeById(cid);
        }else{
            momentsService.removeById(mid);
        }
    }

    @PostMapping("/init")
    @ResponseBody
    public ResInfo loadMoments(HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(CONSTANTS.ACCOUNT.USER_STR);
            session.setAttribute("uid", user.getUserID());
            return setResInfo(user.getUserID());
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/visitFriend")
    @ResponseBody
    public Map<String,Object> visitFriend(@RequestBody Map<String,String> params, HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            long masterId = (Long)session.getAttribute("uid");
            Map<String,Object> map = new HashMap<>();
            if (Objects.equals(params.get("name"),"")){
                map.put("moments",getOrderedLimitedMomentsByUserIds
                        (getFriendsIdsByUserId
                                (true,false,-1,masterId),0,C_moments,"time"));
            }else{
                long friendId = getUserIdByName(params.get("name"));
                if(friendId != masterId){
                    setFriend(masterId,friendId,params.get("time"));
                }
                map.put("moments",getOrderedLimitedMomentsByUserId(friendId,0,C_moments,"time"));
            }
            map.put("lastViewFriends",getOrderedFriendsByUserId(masterId,C_lastViewFriends,"lastViewTime"));
            map.put("mostViewFriends",getOrderedFriendsByUserId(masterId,C_mostIntimacyFriends,"intimacy"));
            return map;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/getMoreMoments")
    @ResponseBody
    public List<ResInfo.moment> getMoreMoments(@RequestBody Map<String,String> params, HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            long uid = (Long)session.getAttribute("uid");
            int start = Integer.parseInt(params.get("count"));
            List<ResInfo.moment> list;
            if (Objects.equals(params.get("name"),"")){
                list = getOrderedLimitedMomentsByUserIds
                        (getFriendsIdsByUserId(true,false,-1,uid),
                                start,C_moments,"time");
            }else{
                long friendId = getUserIdByName(params.get("name"));
                list = getOrderedLimitedMomentsByUserId(friendId,start,C_moments,"time");
            }
            return list;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/getMoreMessages")
    @ResponseBody
    public List<Messages> getMoreMessages(@RequestBody Map<String,Integer> map, HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            return getOrderedLimitedMessagesByUserId(
                    (long)session.getAttribute("uid"),map.get("count"),C_messages,"time");
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/like")
    @ResponseBody
    public List<String> like(@RequestBody Map<String,String> params, HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            long uid = (Long)session.getAttribute("uid");
            int mid = Integer.parseInt(params.get("id1"));
            int cid = Integer.parseInt(params.get("id2"));
            int sid = Integer.parseInt(params.get("id3"));
            String time = params.get("time");
            updateMessage(uid, mid, cid, sid, "点赞",
                    getGetMsgName(mid, cid, sid), time);
            return updateLikePeople(uid, mid, cid, sid);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/addComment")
    @ResponseBody
    public Map<String,Object> addComment(@RequestBody Map<String, String> params, HttpServletRequest request){
        try {
            HttpSession session = request.getSession();
            long uid = (Long)session.getAttribute("uid");
            int mid = Integer.parseInt(params.get("id1"));
            int cid = Integer.parseInt(params.get("id2"));
            String time = params.get("time");
            String content = params.get("content");
            String name2 = params.get("name2");
            Map<String, Object> map = new HashMap<>();
            if (cid!=-1){
                Subcomments obj = addSubComment(uid,cid,time,content,name2);
                int sid = obj.getId();
                String getMsgName =
                        Objects.equals(name2, "") ?getGetMsgName(mid,cid,-1):name2;
                updateMessage(uid,mid,cid,sid,"评论",getMsgName,time);
                map.put("comment", setResInfoSubComment(obj));
            }else{
                Comments obj = addComment(uid, mid,time,content);
                cid = obj.getId();
                String getMsgName = getGetMsgName(mid,-1,-1);
                updateMessage(uid, mid, cid, -1, "评论", getMsgName,time);
                map.put("comment", setResInfoComment(obj));
            }
            return map;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public boolean delete(@RequestBody Map<String,Integer> params){
        try {
            delete(params.get("id1"),params.get("id2"),params.get("id3"));
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/sendMessagesToMentionNames")
    @ResponseBody
    public boolean sendMessagesToMentionNames(@RequestBody Map<String,Object> params, HttpServletRequest request){
        try {
            @SuppressWarnings("unchecked")
            ArrayList<String> names = (ArrayList<String>) params.get("names");
            for (String name:names){
                updateMessage((Long) request.getSession().getAttribute("uid"),
                        (Integer)params.get("id1"),(Integer)params.get("id2"),
                        (Integer)params.get("id3"),
                        "@",name,(String) params.get("time"));
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/getDetails")
    @ResponseBody
    public Map<String,Object> getDetails(@RequestBody Map<String,Integer> params){
        try {
            Messages message = messagesService.getById(params.get("id"));
            if (message.getSeen()==0){
                message.setSeen(1);
                messagesService.updateById(message);
            }
            Map<String,Object> map = new HashMap<>();
            map.put("seen",message.getSeen());
            map.put("newMessagesCount",getNewMessagesCount(getUserIdByName(message.getGetmsgname())));
            List<ResInfo.moment> moment = new ArrayList<>();
            moment.add(setResInfoMoment(momentsService.getById(message.getId1())));
            map.put("moments",moment);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping( "/sendMoments")
    @ResponseBody
    public void sendMoments(@RequestBody Map<String, Object> params, HttpServletRequest request) {
        try {
            Moments moment = new Moments();
            moment.setUid((Long) request.getSession().getAttribute("uid"));
            moment.setTime((String) params.get("time"));
            moment.setContent((String) params.get("content"));
            @SuppressWarnings("unchecked")
            List<String> urls = (List<String>) params.get("imgUrls");
            if(urls.size()>0){
                StringBuilder builder = new StringBuilder();
                for(String url:urls){
                    builder.append(url);
                    builder.append(" ");
                }
                builder.deleteCharAt(builder.length()-1);
                moment.setContentImages(builder.toString());
            }
            momentsService.saveOrUpdate(moment);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/transmit")
    @ResponseBody
    public void transmit(@RequestBody Map<String,Object> params, HttpServletRequest request){
        try {
            Moments moment = new Moments();
            moment.setUid((Long) request.getSession().getAttribute("uid"));
            moment.setTime((String) params.get("time"));
            moment.setContent((String) params.get("content"));
            moment.setTransmitid((Integer)params.get("transmit_id"));
            momentsService.saveOrUpdate(moment);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}