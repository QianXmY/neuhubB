package org.se.neuhub.moments.pojo;

import lombok.*;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Data
public class ResInfo {

    private currentUser currentUser;
    private List<friend> friends;
    private List<friend> lastViewFriends;
    private List<Messages> messages;
    private List<moment> moments;
    private List<friend> mostViewFriends;
    private List<moment> mostViewFriendsMoments;
    private Integer newMessagesCount;

    @Data
    public static class currentUser{
        private String imgsrc;
        private String name;
    }

    @Data
    public static class friend{
        private String imgsrc;
        private int intimacy;
        private String lastViewTime;
        private String name;
    }

    @Data
    public static class comment{
        private Integer id;
        private String name;
        private List<String> likePeople;
        private String time;
        private String imgsrc;
        private String content;
        private List<subComment> sub;

    }

    @Data
    public static class subComment{
        private Integer id;
        private String name;
        private String name2;
        private List<String> likePeople;
        private String time;
        private String imgsrc;
        private String content;


    }

    @Data
    public static class moment{
        private Integer id;
        private String name;
        private String time;
        private String content;
        private List<String> content_images;
        private List<String> likePeople;
        private TransmitMoment transmitMoment;
        private List<comment> comments;
        private String imgsrc;
    }
    @Data
    public static class TransmitMoment{
        private int id;
        private String imgsrc;
        private String name;
        private String content;
        private List<String> content_images;
    }
}
