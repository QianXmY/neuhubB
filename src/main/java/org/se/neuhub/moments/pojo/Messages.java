package org.se.neuhub.moments.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName messages
 */
@TableName(value ="messages")
@Data
public class Messages implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private String type;

    /**
     * 
     */
    private String getmsgname;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private String time;

    /**
     * 
     */

    private Integer seen;

    /**
     * 
     */
    private Integer id1;

    /**
     * 
     */
    private Integer id2;

    /**
     * 
     */
    private Integer id3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}