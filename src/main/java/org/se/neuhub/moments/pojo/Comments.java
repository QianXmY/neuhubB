package org.se.neuhub.moments.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName comments
 */
@TableName(value ="comments")
@Data
public class Comments implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Long uid;

    /**
     * 
     */
    private String time;

    /**
     * 
     */
    private String content;

    /**
     * 
     */
    private String likepeople;

    /**
     * 
     */
    private Integer mid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}