package org.se.neuhub.moments.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName subcomments
 */
@TableName(value ="subcomments")
@Data
public class Subcomments implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Integer cid;

    /**
     * 
     */
    private Long uid;

    /**
     * 
     */
    private String name2;

    /**
     * 
     */
    private String time;

    /**
     * 
     */
    private String content;

    /**
     * 
     */
    private String likepeople;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}