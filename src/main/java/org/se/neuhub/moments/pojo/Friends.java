package org.se.neuhub.moments.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.github.jeffreyning.mybatisplus.anno.MppMultiId;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName friends
 */
@TableName(value ="friends")
@Data
public class Friends implements Serializable {
    /**
     * 
     */
    @MppMultiId
    private Long masterid;

    /**
     * 
     */
    @MppMultiId
    private Long friendid;

    /**
     * 
     */
    private Integer intimacy;

    /**
     * 
     */
    private String lastviewtime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}