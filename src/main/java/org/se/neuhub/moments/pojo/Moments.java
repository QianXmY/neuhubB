package org.se.neuhub.moments.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName moments
 */
@TableName(value ="moments")
@Data
public class Moments implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    private Long uid;

    /**
     * 
     */
    private String time;

    /**
     * 
     */
    private String content;

    /**
     * 
     */
    private String contentImages;

    /**
     * 
     */
    private String likepeople;


    /**
     * 
     */
    private Integer transmitid;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}