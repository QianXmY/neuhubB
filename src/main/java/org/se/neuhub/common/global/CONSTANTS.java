package org.se.neuhub.common.global;

/**
 * Created by IntelliJ IDEA.
 * Description: 全局常量们
 * Author: 白千一
 * DateTime: 2022/4/8 21:21
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

public class CONSTANTS {


    public static class ACCOUNT{
        public static final String USER_STR = "USER";
        public static final String USERNAME_STR = "USERNAME";
        public static final String EMAIL_REG = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        public static final String EMAIL_STR = "email";
        public static final String CHECKOUT_CODE_STR = "checkoutCode";
    }


    /**
     *  请求返回码。和org/se/neuhub/common/Result.java配合使用
     */
    public static class RESPONSE_CODE{
        public static final int SUCCESS = 1;
        public static final int UNKNOWN_ERROR = 0;
        public static final int FAIL = -1;
        public static final int NOT_LOGGED = -2;    // 未登录。拦截需要登录才能访问的接口时使用
        public static final int FIND_LOGGER_FAIL = -3;   // 找不到本该登陆的人员的信息
        public static final int ERROR_CODE = -4;    // 验证码错误
        public static final int UPDATE_FAIL = -5;    // 数据库更新失败
    }

}
