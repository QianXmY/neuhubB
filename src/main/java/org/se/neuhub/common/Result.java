package org.se.neuhub.common;


import org.se.neuhub.common.global.CONSTANTS.RESPONSE_CODE;

/**
 * Created by IntelliJ IDEA.
 * Description: 接口的返回数据
 * Author: 白千一
 * DateTime: 2022/4/8 17:45
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

public class Result{

    int code;  // 请求是否成功反馈
    String message; // 返回信息
    Object data;    // 响应数据

    private Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

    public static Result success(String message, Object data){
        return new Result(RESPONSE_CODE.SUCCESS,message, data);
    }

    public static Result success(String message){
        return new Result(RESPONSE_CODE.SUCCESS, message, null);
    }

    public static Result success(){
        return new Result(RESPONSE_CODE.SUCCESS, "successful request", null);
    }

    public static Result fail(String message){
        return new Result(RESPONSE_CODE.FAIL, message, null);
    }

    public static Result fail(){
        return new Result(RESPONSE_CODE.FAIL, "fail request", null);
    }

    /**
     * 自定义返回结果
     * @param code 状态码
     * @param message 信息
     * @param data 数据
     * @return Result
     */
    public static Result customize(int code, String message, Object data){
        return new Result(code, message, data);
    }
}
