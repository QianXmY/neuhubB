package org.se.neuhub.common.util;

import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/4/12 13:18
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

public class TextMaker {

    private static final String letterRepository = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String randomText(int length) {
        return randomText(letterRepository, length);
    }

    public static String randomText(String letterRepository, int length){
        int repoLength = letterRepository.length();
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(repoLength);
            sb.append(letterRepository.charAt(number));
        }
        return sb.toString();
    }

}
