package org.se.neuhub.common.util;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Description: 用于图片的上传和删除，使用七牛云的服务器，所有资源可以通过公网访问
 * Author: 周靖森
 * DateTime: 2022/5/3
 */

public class ImageUtils {
    //构造一个带指定 Region 对象的配置类
    private final Configuration cfg = new Configuration(Region.region1());
    //上传凭证
    private final String accessKey = "_zG9th2zomN1iNcLCtJ0El80oJ_rvcHBTtv9o43X";
    private final String secretKey = "t6JMbTSHUrNCCU1K96qEaZWKErBVTtODR0mvnfos";
    private final String bucket = "neuhub";

    private static final String net = "http://rb99yvmfm.hb-bkt.clouddn.com/";

    public ImageUtils(){}

    /**
     *
     * @param multipartFile 用于上传的图片
     * @param key 图片文件名，可以为null，此时以文件内容的hash值作为文件名
     * @return 图片的文件名，完整的url请调用getUrl方法
     */
    public String upload(MultipartFile multipartFile, String key){
        UploadManager uploadManager = new UploadManager(cfg);
        try {
            Auth auth = Auth.create(accessKey, secretKey);
            String upToken = auth.uploadToken(bucket);
            try {
                Response response = uploadManager.put(multipartFile.getBytes(), key, upToken);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                return putRet.key;
            } catch (QiniuException ex) {
                Response r = ex.response;
                System.err.println(r.toString());
                try {
                    System.err.println(r.bodyString());
                } catch (QiniuException ex2) {
                    ex2.printStackTrace();
                }
            }
        } catch (IOException ex1) {
            ex1.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param key 图片文件名
     * @return 图片的url
     */
    public static String getUrl(String key){
        return net + key;
    }

    /**
     *
      * @param key 图片的文件名
     * @return 相关信息，一般情况下可以忽略
     */
    public Response delete(String key) {
        try{
            Auth auth = Auth.create(accessKey, secretKey);
            BucketManager bucketManager = new BucketManager(auth,cfg);
            Response response = bucketManager.delete(bucket, key);
            int retry = 0;
            while (response.needRetry() && retry++ < 3) {
                response = bucketManager.delete(bucket, key);
            }
            return response;
        }catch (QiniuException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
