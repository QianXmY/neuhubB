package org.se.neuhub.common.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/4/8 18:05
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

@Configuration
public class WebMvcConf implements WebMvcConfigurer {

    private LoginInterceptor loginInterceptor;

    @Autowired
    public void setLoginInterceptor(LoginInterceptor loginInterceptor) {
        this.loginInterceptor = loginInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry){
        // 下面的list里是放行路径
        registry.addInterceptor(loginInterceptor).excludePathPatterns(
                Arrays.asList("/user/login", "/user/register", "/user/reset", "/user/confirm")
        );
    }

}
