package org.se.neuhub.common.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.se.neuhub.common.Result;
import org.se.neuhub.common.global.CONSTANTS;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by IntelliJ IDEA.
 * Description: 对于一些只有登录才能使用的接口进行验证。
 * Author: 白千一
 * DateTime: 2022/4/8 17:41
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if(session == null || session.getAttribute(CONSTANTS.ACCOUNT.USER_STR) == null){  // 请求session为空或session内部不含user实例，说明用户未登录
            response.getWriter().write(
                    new ObjectMapper().writeValueAsString(
                            Result.customize(CONSTANTS.RESPONSE_CODE.NOT_LOGGED,"登录才能使用", null)
                    )
            );
            return false;
        }
        return true;
    }



}
