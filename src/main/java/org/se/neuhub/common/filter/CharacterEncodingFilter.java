package org.se.neuhub.common.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Description: 编码过滤器
 * Author: 白千一
 * DateTime: 2022/3/27 23:38
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

@WebFilter(urlPatterns = "/*")
public class CharacterEncodingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig){
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding("utf-8");
        servletResponse.setContentType("text/html");
        servletResponse.setCharacterEncoding("utf-8");
        filterChain.doFilter(servletRequest, servletResponse);
//        System.out.println("进入编码过滤器");
    }

    @Override
    public void destroy() {
    }
}