package org.se.neuhub.account.service.Impl;

import org.se.neuhub.account.dao.UserDao;
import org.se.neuhub.account.entity.User;
import org.se.neuhub.account.service.UserService;
import org.se.neuhub.common.global.CONSTANTS.ACCOUNT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/3/31 20:51
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */


@Service("userService")
public class UserServiceImpl implements UserService {


    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    @Override
    public User login(String username, String password) {
        User user =  userDao.queryByUsername(username);
        if(user != null && !password.equals(user.getPassword())){
            user = null;
        }
        return user;
    }

    @Override
    public User register(String username, String password, String email){
        User user = new User();
        user.setUsername(username); user.setPassword(password); user.setEmail(email);
        try{
            if(Pattern.matches(ACCOUNT.EMAIL_REG, email)){
                userDao.insert(user);   // 重复的话会抛出重复异常
            }else{
                throw new Exception();
            }
        }catch(Exception e){
            user = null;
        }
        return user;
    }

    @Override
    public String getEmailAddress(String username){
        User user = userDao.queryByUsername(username);
        if(user == null) return null;
        return user.getEmail();

    }

    @Override
    public boolean changePassword(String username, String newPassword) {
        User user = userDao.queryByUsername(username);
        user.setPassword(newPassword);
        return userDao.update(user) > 0;
    }

    @Override
    public boolean changePassword(User user){
        return userDao.update(user) > 0;
    }
}
