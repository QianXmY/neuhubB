package org.se.neuhub.account.service.Impl;

import org.se.neuhub.account.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/4/12 11:34
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

@Service("emailService")
public class EmailServiceIml implements EmailService {

    @Resource
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean sendEmail(String toMail, String senderName, String subject, String content){
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
        try{
            senderName=javax.mail.internet.MimeUtility.encodeText(senderName);
        }catch (UnsupportedEncodingException e){
            senderName = "";
        }
        try {
            message.setFrom(new InternetAddress(senderName+" <"+from+">"));
            message.setTo(toMail);
            message.setSubject(subject);
            message.setText(content, true);
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


}
