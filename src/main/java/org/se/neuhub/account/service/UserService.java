package org.se.neuhub.account.service;


import org.se.neuhub.account.entity.User;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/3/31 20:51
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

public interface UserService {

    /**
     * login in
     * @param username 登陆所需用户名
     * @param password 密码
     * @return User
     */
    User login(String username, String password);

    /**
     * register for the account
     * @param username 用户名
     * @param password 密码
     * @param email 邮箱
     * @return User
     */
    User register(String username, String password, String email);

    /**
     * @param username user who want to reset his password
     * @return the email address
     */
    String getEmailAddress(String username);


    /**
     * change the password
     * @param username  user who want to change his password
     * @param newPassword the new password string that the user want to change to
     * @return if the change is success
     */
    boolean changePassword(String username, String newPassword);

    boolean changePassword(User user);

}
