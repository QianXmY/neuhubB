package org.se.neuhub.account.service;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/4/12 14:37
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

public interface EmailService {

    /**
     *
     * @param to email address of receiver
     * @param senderName name of sender
     * @param subject subject of email
     * @param content content of email
     * @return
     */
    boolean sendEmail(String to, String senderName, String subject, String content);

}
