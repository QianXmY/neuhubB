package org.se.neuhub.account.controller;

import org.se.neuhub.account.entity.User;
import org.se.neuhub.account.service.EmailService;
import org.se.neuhub.account.service.UserService;
import org.se.neuhub.common.Result;
import org.se.neuhub.common.global.CONSTANTS.RESPONSE_CODE;
import org.se.neuhub.common.global.CONSTANTS.ACCOUNT;
import org.se.neuhub.common.util.ImageUtils;
import org.se.neuhub.common.util.IpUtil;
import org.se.neuhub.common.util.TextMaker;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Description:
 * Author: 白千一
 * DateTime: 2022/3/31 20:54
 * Copyright (c) 2022 BaiQianYi All rights reserved.
 */

@RestController
@RequestMapping("user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private EmailService emailService;

    @Resource
    private SpringTemplateEngine templateEngine;    // 构建email的html文本

    @GetMapping("login")
    public Result login(HttpServletRequest request, String username, String password){
//        System.out.println("进入");
        User user = userService.login(username, password);
        if(user == null){   // 用户不存在或者密码错误
            return Result.fail("login fail");
        }
        HttpSession session = request.getSession();
        session.setAttribute(ACCOUNT.USER_STR, user);
        return Result.success("login successfully!");
    }

    @PostMapping("register")
    public Result register(
            HttpServletRequest request,
            String username,
            String password,
            String email
    ){
        User user = userService.register(username, password, email);
        if(user == null){
            return Result.fail("register fail");
        }
        HttpSession session = request.getSession();
        session.setAttribute(ACCOUNT.USER_STR, user);
        return Result.success("register successfully");
    }

    @GetMapping("logout")
    public Result logout(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session != null){
            session.invalidate();
            return Result.success("成功注销");
        }
        return Result.fail("有用户未经登录使用此功能");
    }

    /**
     * confirm the identity of someone who want to reset its password
     * @param request httpServletRequest
     * @param username username
     * @return response result
     */
    @GetMapping("confirm")
    public Result confirmIdentity(HttpServletRequest request, String username) {
        String emailAddress = userService.getEmailAddress(username);    // 获得用户邮箱地址
        if (emailAddress == null) {
            return Result.fail("user not exists!");
        }
        HttpSession session = request.getSession();
        session.setAttribute(ACCOUNT.USERNAME_STR, username);
        session.setAttribute(ACCOUNT.EMAIL_STR, emailAddress);
        String emailMarkedCode;
        try {
            emailMarkedCode = emailAddress.substring(0, 4) + "****" + emailAddress.substring(emailAddress.indexOf("@"));
        } catch (StringIndexOutOfBoundsException e) {
            emailMarkedCode = emailAddress;
        }
        return Result.success(emailMarkedCode);
    }

    /**
     * If someone forgets the old password and still want to change the password,
     * Web should ask it for the checkCode gotten from its email.
     * This method get a checkCode for the user and email it with the code.
     * @param request httpServletRequest
     * @return response result
     */
    @GetMapping("reset")
    public Result reset(HttpServletRequest request){
        String ipAddress = IpUtil.getIpAddress(request);    // 获得请求IP地址
        String checkCode = TextMaker.randomText(6); // 生成验证码
        // 利用 Thymeleaf 模板构建 html 文本
        Context ctx = new Context();
        ctx.setVariable("checkCode", checkCode);
        ctx.setVariable("ipAddress", ipAddress);
        String emailText = templateEngine.process("email", ctx);    // 获得html文本
        HttpSession session = request.getSession(false);
        if(session == null || session.getAttribute(ACCOUNT.USERNAME_STR) == null){
            return Result.customize(RESPONSE_CODE.FIND_LOGGER_FAIL, "没有人员信息", null);
        }
        session.setMaxInactiveInterval(10 * 60);    // 设置对话验证的有效期期为10分钟
        String emailAddress = (String)session.getAttribute(ACCOUNT.EMAIL_STR);    // 获得用户邮箱地址
        if(!emailService.sendEmail(emailAddress, "NEUHUB","Reset Password", emailText)){
            return Result.fail("send email fail");
        }
        session.setAttribute(ACCOUNT.CHECKOUT_CODE_STR, checkCode);
        return Result.success("send mail success");
    }


    @PostMapping("reset")
    public Result reset(HttpServletRequest request, String checkCode, String newPassword){
        HttpSession session = request.getSession(false);
        String username;
        if(session == null || (username = (String)session.getAttribute(ACCOUNT.USERNAME_STR)) == null){
            return Result.customize(RESPONSE_CODE.FIND_LOGGER_FAIL, "没有人员信息", null);
        }
        if(!session.getAttribute(ACCOUNT.CHECKOUT_CODE_STR).equals(checkCode)){
            return Result.customize(RESPONSE_CODE.ERROR_CODE, "验证码错误", null);
        }
        if(userService.changePassword(username, newPassword)){
            session.invalidate();
            return Result.success();
        }
        return Result.fail("密码更新失败");
    }


    @PostMapping("change")
    public Result changePassword(HttpSession session, String oldPassword, String newPassword){
        User user = (User)session.getAttribute(ACCOUNT.USER_STR);
        if(user.getPassword().equals(oldPassword)){
            user.setPassword(newPassword);
            if(userService.changePassword(user)){
                session.setAttribute(ACCOUNT.USER_STR, user);
                return Result.success();
            }
            return Result.customize(RESPONSE_CODE.UPDATE_FAIL, "密码更新失败", null);
        }
        return Result.fail("密码错误");
    }

    @PostMapping("avatarUpload")
    @ResponseBody
    public Map<String, String> upload(HttpSession session, @RequestParam("file") MultipartFile multipartFile){
        String username = ((User)session.getAttribute(ACCOUNT.USER_STR)).getUsername();
        System.out.println("收到请求");
        try {
            ImageUtils imageUtils = new ImageUtils();
            System.out.println(imageUtils.delete(username));
            String name = imageUtils.upload(multipartFile, username);
            Map<String, String> map = new HashMap<>();
            map.put("name", name);
            map.put("url", ImageUtils.getUrl(name));
            System.out.println(map);
            return map;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
