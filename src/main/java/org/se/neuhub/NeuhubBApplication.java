package org.se.neuhub;

import com.github.jeffreyning.mybatisplus.conf.EnableMPP;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;


@ServletComponentScan
@SpringBootApplication
@EnableMPP
public class NeuhubBApplication {

    public static void main(String[] args) {
        SpringApplication.run(NeuhubBApplication.class, args);
    }

}
