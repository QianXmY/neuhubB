package org.se.neuhub.friends.entity.service.impl;/**
 * @author xuelinwei
 * @create 2022/5/4 下午4:51
 */

import org.se.neuhub.account.entity.User;
import org.se.neuhub.friends.entity.dto.ServiceResult;
import org.se.neuhub.friends.entity.po.Friend;
import org.se.neuhub.friends.entity.service.FriendService;

import org.se.neuhub.friends.entity.service.ServiceMessage;
import org.se.neuhub.friends.entity.service.Status;

import org.se.neuhub.friends.others.Message;

import java.util.LinkedList;
import java.util.List;

import static org.se.neuhub.friends.entity.service.ServiceMessage.*;

/**
 * @program: neuhubB
 * @description: 好友相关服务,由于DAO未实现，方法未写完
 * @author: xuelinwei
 * @create: 2022/5/4
 **/
public class FriendServiceImpl implements FriendService {

    @Override
    public ServiceResult addFriend(Friend friend) {
        if (friend == null) {
            return new ServiceResult(Status.ERROR, PARAMETER_NOT_ENOUGH.message, friend);
        }
        //检测是否自己添加自己
        if(friend.getFriendId()== friend.getUserId()){
            return new ServiceResult(Status.ERROR, CANNOT_ADD_YOURSELF.message, friend);
        }
        //检测是否已经已经是好友
        if(isFriend(friend)){
            return new ServiceResult(Status.ERROR, ALREADY_FRIEND.message, friend);
        }
        //发送好友通知
        Message message=new Message();

        //等待实现，不知道如何使用DAO接口
        try {
            /*

            //检查是否添加过好友，不可重复添加

            //检查好友是否存在

            //检查用户是否存在

            //设置好友默认备注为好友昵称

            //添加好友

            //添加好友后如果已经双向加好友，则不用发申请，而是把之前的申请消息删除
            if (isFriend(friend)) {
                //查询自己与系统建立的聊天,用于查找发送好友申请那条消息并删除

            } else {
                //如果对方是普通用户，需要发送加好友通知
                if () {
                    //获取好友与系统的聊天，用于推送通知

                   //生成一条加好友通知消息

                }
            }
             */

        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, friend);
        }
        return new ServiceResult(Status.SUCCESS, ADD_FRIEND_SUCCESS.message, friend);

    }

    @Override
    public ServiceResult listFriend(Object userId) {

        if(userId==null){
            return new ServiceResult(Status.ERROR,PARAMETER_NOT_ENOUGH.message,userId);
        }
        List<Friend>friendList=new LinkedList<>();
        try {
            //缺少DAO和实现
        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, userId);
        }

        return new ServiceResult(Status.SUCCESS,null,friendList);
    }

    @Override
    public ServiceResult listMyFollow(Object userId) {
        if(userId==null){
            return new ServiceResult(Status.ERROR, PARAMETER_NOT_ENOUGH.message, userId);
        }
        List<Friend>list=new LinkedList<>();
        try {
            //缺少DAO实现
            //if(list.size()==0)
            // return new ServiceResult(Status.SUCCESS, NO_FAN_NOW.message, userId);

        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, userId);
        }
        return new ServiceResult(Status.SUCCESS,null,list);
    }

    @Override
    public ServiceResult listMyfans(Object userId) {
        if(userId==null){
            return new ServiceResult(Status.ERROR,PARAMETER_NOT_ENOUGH.message,userId);
        }

        List<User>userList=new LinkedList<>();

        try {
            //缺少DAO，具体实现
            //List<Friend> list=
            //if(list.size()==0)
            // return new ServiceResult(Status.SUCCESS, NO_FAN_NOW.message, userId);

            //返回粉丝列表

        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, userId);
        }
        return new ServiceResult(Status.SUCCESS, DELETE_FRIEND_SUCCESS.message,userId);
    }

    @Override
    public ServiceResult updateFriend(Friend friend) {
        if(friend==null){
            return new ServiceResult(Status.ERROR,PARAMETER_NOT_ENOUGH.message,friend);
        }
        try {
            //检差是否存在该friend关系
            //更新friend

        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, friend);
        }
        return new ServiceResult(Status.SUCCESS, UPDATE_FRIEND_SUCCESS.message,friend);
    }

    @Override
    public ServiceResult removeFriend(Friend friend) {


        if(friend==null){
            return new ServiceResult(Status.ERROR,PARAMETER_NOT_ENOUGH.message,friend);
        }
        try {
            //检差是否存在该friend关系
                //移除friend

        }
        catch (Exception e){
            e.printStackTrace();
            return new ServiceResult(Status.ERROR, DATABASE_ERROR.message, friend);
        }
        return new ServiceResult(Status.SUCCESS, DELETE_FRIEND_SUCCESS.message,friend);
    }

    @Override
    public boolean isFriend(Friend friend) {
        /**
         * 等待实现DAO
         */
        try {
            //return false;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean isFriend(Object userId, Object friendId) {

        Friend friend=null;

        /**
         * 等待实现DAO
         */
        try {
            //return false;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }
}
