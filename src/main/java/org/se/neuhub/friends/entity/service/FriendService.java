package org.se.neuhub.friends.entity.service;

import org.se.neuhub.friends.entity.dto.ServiceResult;
import org.se.neuhub.friends.entity.po.Friend;

/**
 * 负责好友相关服务
 *
 * @author xuelinwei
 * @create 2022/5/4 下午3:15
 */
public interface FriendService {
    /**
     * 添加好友关系
     *
     * @param friend 添加的好友关系
     * @return
     */
    ServiceResult addFriend(Friend friend);



    /**
     * 返回一个用户的好友列表
     *
     * @param userId 用户id
     * @return
     */
    ServiceResult listFriend(Object userId);

    /**
     * 返回一个用户的关注列表
     * @param userId 用户id
     * @return
     */
    ServiceResult listMyFollow(Object userId);

    /**
     * 返回一个用户的粉丝（关注他的人）列表
     * @param userId 用户
     * @return
     */
    ServiceResult listMyfans(Object userId);

    /**
     * 更新好友信息
     * @param friend 要更新的好友关系
     * @return
     */
    ServiceResult updateFriend(Friend friend);


    /**
     * 移除好友关系
     * @param friend 要移除的好友关系
     * @return
     */
    ServiceResult removeFriend(Friend friend);


    /**
     * 查询是否存在一条这样的好友关系
     * @param friend 判断好友关系是否双向
     * @return
     */
    boolean isFriend(Friend friend);


    /**
     * 查询是否userId和friendId是好友
     * @param userId 用户id
     * @param friendId 朋友id
     * @return
     */
    boolean isFriend(Object userId,Object friendId);
}
