package org.se.neuhub.friends.entity.service;

/**
 * 枚举变量 各种错误
 *
 * @author xuelinwei
 * @create 2022/5/4 下午9:25
 */
public enum ServiceMessage {
    /**
     * **************************************
     * 好友服务信息
     * **************************************
     */


    /**
     * 添加关注成功
     */
    ADD_FRIEND_SUCCESS("添加关注成功,在您的关注列表中可以看到该用户，当对方也关注你时，可在好友列表中看到对方"),


    /**
     * 不可重复关注
     */
    ADD_FRIEND_AGAIN("您已经关注该用户，不可重复关注"),

    NO_FRIEND_NOW("现在还没有好友，快去添加吧"),

    NO_FOLLOW_NOW("现在没有关注的人"),

    NO_FAN_NOW("现在没有粉丝"),

    DELETE_FRIEND_SUCCESS("对方已从你的关注中移除，不再接收对方消息"),

    FRIEND_NOT_EXIST("该用户不存在"),

    UPDATE_FRIEND_SUCCESS("更新好友信息成功"),

    ALREADY_FRIEND("已经是好友，不可重复添加"),

    CANNOT_ADD_YOURSELF("不能关注自己"),

    AGAIN_FRIEND("双向关注，达成好友，可以聊天"),

    CANNOT_DELETE_SYSTEM("不能删除系统帐号"),

    /**
     * **************************************
     * 系统消息
     * **************************************
     */


    PARAMETER_NOT_ENOUGH("参数不足，无法执行"),
    PLEASE_REDO("执行失败，请重新执行"),
    OPPERATE_SUCCESS("执行成功"),

    /**
     * 系统故障
     */
    DATABASE_ERROR("系统数据库发生了故障，无法正常提供服务");;



    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ServiceMessage(String message) {
        this.message = message;
    }
}
