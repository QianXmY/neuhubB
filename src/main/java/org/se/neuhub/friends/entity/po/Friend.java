package org.se.neuhub.friends.entity.po;

import org.se.neuhub.friends.entity.abs.BaseEntity;

import java.sql.Timestamp;

/**
 * @author xuelinwei
 */

/*
 * 好友关系
 * 添加了 intimacy，lastViewTime
 * */
public class Friend extends BaseEntity {
    private int userId;
    private int friendId;
    private int charId;
    private String photo;
    private int groupId;
    private String alias;
    private String description;

    private int intimacy;
    private Timestamp lastViewTime;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFriendId() {
        return friendId;
    }

    public void setFriendId(int friendId) {
        this.friendId = friendId;
    }

    public int getCharId() {
        return charId;
    }

    public void setCharId(int charId) {
        this.charId = charId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean setIntimacyByUserID(int intimacy, int userID1, int userID2) {
        return false;
    }

    public int getIntimacyByUserID(int intimacy, int userID1, int userID2) {
        return 0;
    }

    public boolean setLastViewTimeByUserID(int userID1, int userID2) {
        return true;
    }

    public Timestamp getLastViewTimeByUserID(int userID1, int userID2) {
        return new Timestamp(System.currentTimeMillis());
    }


}
