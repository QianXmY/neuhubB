package org.se.neuhub.friends.entity.abs;/**
 * @author xuelinwei
 * @create 2022/5/4 下午3:29
 */

import java.sql.Timestamp;

/**
 * @program: neuhubB
 * @description: 好友关系基类
 * @author: xuelinwei
 * @create: 2022/5/4
 **/
public abstract class BaseEntity {

    private int id;
    private int status;
    private Timestamp gmt_Create;
    private Timestamp gmt_Modified;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getGmt_Create() {
        return gmt_Create;
    }

    public void setGmt_Create(Timestamp gmt_Create) {
        this.gmt_Create = gmt_Create;
    }

    public Timestamp getGmt_Modified() {
        return gmt_Modified;
    }

    public void setGmt_Modified(Timestamp gmt_Modified) {
        this.gmt_Modified = gmt_Modified;
    }

}
