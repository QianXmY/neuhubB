# NEUHUB_B

#### 介绍

NEUHUB（东大社交圈子）后端

#### 开发环境

- java 14.0.2
- SpringBoot v2.6.4
- MyBatis-plus 3.5.1
- MySQL 8.0.26
- maven 3.6.3


#### 开发工具

- IntelliJ IDEA
- ApiFox 


#### 运行环境要求

- Java 8

#### 使用说明

1. 安装数据库

    首先运行`SQL`目录下的`neuhub.sql`文件，然后依次运行其他文件，建立本项目的数据库表

2. 端口说明

    本项目默认使用8080端口运行，若此端口**被占用**，则无法正常运行。

3. idea运行（开发模式）

   1. 使用IDEA打开本目录，然后等待idea自动配置环境完成。 
   2. 修改`src/main/resources/application`文件，将其中的`spring.datasource.username`和`spring.datasource.password`字段修改成**本机**数据库对应的用户名和密码。 
   3. 点击运行

4. jar包运行（生产模式）

   使用终端进入文件`neuhubB-0.0.1-SNAPSHOT.jar`同一目录下，运行指令：
   ```shell
   java -jar ./neuhubB-0.0.1-SNAPSHOT.jar
   ```
   即可开启项目。项目开启时**终端不可关闭**，终端关闭即停止项目运行。

_idea运行和jar运行选其一即可，同时运行不是必须条件。_


